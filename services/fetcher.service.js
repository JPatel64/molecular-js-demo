"use strict";
const got = require("got");
const uuidv4 = require("uuid");

module.exports = {
	name: "fetcher",
	actions: {
		devices: {
			params: {
				num: { type: "number", optional: true }
			},
			async handler(ctx) {
				let dcpRequest = {
					"transactionType" : "UPGRADE",
					"productTypes" : ["DEVICE"]
				};
    
				let devices = await this.getDevices(dcpRequest);
				return this.formatDevices(JSON.parse(devices.body));
			}
		},

		accessories: {
			params: {
				num: { type: "number", optional: true }
			},
			async handler(ctx) {
				let dcpRequest = {
					"transactionType" : "UPGRADE",
					"productTypes" : ["ACCESSORY"]
				};
    
				let devices = await this.getDevices(dcpRequest);
				return this.formatDevices(JSON.parse(devices.body));
			}
		},
	},
	methods: {
		async getDevices(request){
			const authToken =
                "eyJraWQiOiI0Nzc1M2UzZi0zOGFjLTQ1ODQtZDcxYy0zZDgxYTE1Nzk5NWMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJBejR0MWdacmVDYTVHRTd5RmE1N0dwYUsyNWlwa2R0aiIsInJ0Ijoie1wic2VnbWVudGF0aW9uSWRcIjpcIlRJVEFOXCJ9IiwiZGVhbGVyQ29kZSI6IiIsImlzcyI6Imh0dHBzOlwvXC9xYXQwMS5hcGkudC1tb2JpbGUuY29tXC9vYXV0aDJcL3Y2IiwibWFzdGVyRGVhbGVyQ29kZSI6IiIsImF1dGhUaW1lIjoiMTYzNzY3ODYzOTg1NiIsInN0b3JlSWQiOiIiLCJ1c24iOiJiMjFmMDJiNi1mZDhiLTE4NGYtMjdmZS1mZDcyMDU2NDcwZWQiLCJhdWQiOiJBejR0MWdacmVDYTVHRTd5RmE1N0dwYUsyNWlwa2R0aiIsInNlbmRlcklkIjoiIiwibmJmIjoxNjM3Njc4NjQwLCJzY29wZSI6IiIsImFwcGxpY2F0aW9uSWQiOiJBQ1VJIiwiZXhwIjoxNjM3NjgyMjQwLCJpYXQiOjE2Mzc2Nzg2NDAsImNoYW5uZWxJZCI6IiIsImp0aSI6IjJhMjczODk3LTAwMzktZTYyOS1lOTc1LWNkOWE5MWExMjNkZiJ9.t9fwIGrfb4X_ihGl23OWFDkt4Z4_XFT-2sYmF4WPtwy_pUGK28bHx20iXjGGl7S0rywqbxwvKnHGQPMgslhwoMMlgw5lMMgjMbgWWlMG6VagQQtHlDRZUyH0GD7QUjqQOS9TENIsDdXLC84fvu1sYrNUHsE4Ihw415qksVm_kpE-SS1br2pM9seuB8opg3VMssLVy30eqDBwsVvMdVK1VLB1s2dAvBwamvBc2kOS5WVgrfzWswYen24Q84n4CyWsVpt7rergZ2d0bhGnRaehIhqwUiM6SxzgLKs2GSxJE0tWKVmiw_evqlKs8OONluxoQHgZEDEk7Xz_WEGb41xSQQ";

			return got.post("https://qat02.pro.api.t-mobile.com/commerce/v3/products", {
				json: request,
				headers: this.getHeaders(authToken),
			});
		},

		findThumbnailImg(sku) {
			const thumbnail = sku.images.find((image) => image.imageType === "ThumbNail");
			if (thumbnail) {
				return thumbnail;
			} else {
				return sku.images[0];
			}
		},

		formatDevices(devices) {
			const deviceMapping = devices.products.map((el) => {
				const sku = el.skus[0];
				return {
					familyName: el.familyName,
					manufacturer: el.manufacturer,
					fromDate: sku.availability.estimatedShippingFromDateTime,
					toDate: sku.availability.estimatedShippingToDateTime,
					price: sku.skuPrices[0].skuPrice.priceSummary.payNowPrice.listPrice.amount,
					thumbNail: this.findThumbnailImg(sku),
					skuId: sku.id,
					skuCode: sku.skuCode,
					availability: sku.availability.availabilityStatus,
				};
			});
		
			return deviceMapping;
		},
        
		getHeaders(authToken){
			return {
				authorization: `Bearer ${authToken}`,
				applicationid: "ATLAS",
				channelid: "CARE",
				interactionid: uuidv4.v4(),
				"x-auth-originator": `${authToken}`,
			};
		},
	},
};