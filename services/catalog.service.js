"use strict";

const { MoleculerError } = require("moleculer").Errors;
const path = require("path");
const pug = require("pug");

module.exports = {
	name: "catalog",
	settings: {
		port: process.env.PORT || 4000,
		publicDir: "",
		baseDir: "",
	},

	actions:{
		devices: {
			rest: "/devices",
			async handler(ctx) {
				ctx.meta.$responseType = "text/html";
				const data = await this.broker.call("fetcher.devices");
				const template = pug.renderFile(path.join(this.settings.baseDir, "index.pug"),  { data, template:"device"});
				return template;
			}
		},

		accessories: {
			rest: "/accessories",
			async handler(ctx) {
				ctx.meta.$responseType = "text/html";
				const data = await this.broker.call("fetcher.accessories");
				const template = pug.renderFile(path.join(this.settings.baseDir, "index.pug"),  { data, template:"accessory"});
				return template;
			}
		},
	},

	methods: {
		/**
		 *
		 * @param {Request} req
		 * @param {Response} res
		 */
		async buildCatalog() {
			try {
				const data = await this.broker.call("fetcher.devices");
				const template =  pug.renderFile("index", {basedir: this.settings.baseDir, data} );
				return template;
			} catch (error) {
				return this.handleErr(error);
			}
		},

		
		/**
		 * @param {Response} res
		 */
		handleErr(res) {
			return err => {
				this.logger.error("Request error!", err);
				res.status(err.code || 500).send(err.message);
			};
		}
	},

	created() {
	},

	started() {
		const baseFolder = path.join(__dirname, "..");
		this.settings.publicDir = path.join(baseFolder, "public");
		this.settings.baseDir = path.join(baseFolder, "views");
	},

	stopped() {

	}
};
