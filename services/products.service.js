"use strict";

const DbMixin = require("../mixins/db.mixin");
const uuid = require("uuid");
const got = require("got");

module.exports = {
	name: "products",
	// version: 1

	/**
	 * Mixins
	 */
	mixins: [DbMixin("products")],

	/**
	 * Settings
	 */
	settings: {
		// Available fields in the responses
		fields: [
			"_id",
			"description",
			"manufacturer",
			"type",
			"colorOptions",
			"memoryOptions",
			"prices",
			"images"
		],

		// Validator for the `create` & `insert` actions.
		entityValidator: {
			name: "string|min:3",
			price: "number|positive"
		}
	},

	/**
	 * Action Hooks
	 */
	hooks: {
		before: {
			/**
			 * Register a before hook for the `create` action.
			 * It sets a default value for the quantity field.
			 * 
			 * @param {Context} ctx 
			 */
			create(ctx) {
				ctx.params.quantity = 0;
			}
		}
	},
	
	/**
	 * Actions
	 */
	actions: {
		/**
		 * The "moleculer-db" mixin registers the following actions:
		 *  - list
		 *  - find
		 *  - count
		 *  - create
		 *  - insert
		 *  - update
		 *  - remove
		 */

		// --- ADDITIONAL ACTIONS ---
	},

	/**
	 * Methods
	 */
	methods: {
		/**
		 * Loading sample data to the collection.
		 * It is called in the DB.mixin after the database
		 * connection establishing & the collection is empty.
		 */
		async seedDB() {
			let dcpRequest = {
				"transactionType" : "UPGRADE",
				"productTypes" : ["DEVICE"]
			};

			let devices = await this.getDevices(dcpRequest);
			let formattedDevices = devices.body.products.map(product => {
				return{
					description: product.description,
					manufacturer: product.manufacturer,
					type: "DEVICE",
					colorOptions: product.colorOptions,
					memoryOptions: product.memoryOptions,
					prices: product.skuPrices,
					images: product.images
				};
			});


			await this.adapter.insertMany(formattedDevices);
		},

		async getDevices(request){
			return got.post("https://qat02.pro.api.t-mobile.com/commerce/v3/products", {
				json: request,
				headers: this.getHeaders(),
			});
		},

		getHeader(){
			return {
				authorization: "Bearer eyJraWQiOiI0Nzc1M2UzZi0zOGFjLTQ1ODQtZDcxYy0zZDgxYTE1Nzk5NWMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJBejR0MWdacmVDYTVHRTd5RmE1N0dwYUsyNWlwa2R0aiIsInJ0Ijoie1wic2VnbWVudGF0aW9uSWRcIjpcIlBPTEFSSVNcIn0iLCJkZWFsZXJDb2RlIjoiIiwiaXNzIjoiaHR0cHM6XC9cL3FhdDAxLmFwaS50LW1vYmlsZS5jb21cL29hdXRoMlwvdjYiLCJtYXN0ZXJEZWFsZXJDb2RlIjoiIiwiYXV0aFRpbWUiOiIxNjM0ODM0OTMxMTY3Iiwic3RvcmVJZCI6IiIsInVzbiI6IjZmOTRiNTE4LWFiYzUtYTgyYy01NzZjLWE4MjcwZGRmM2M0ZiIsImF1ZCI6IkF6NHQxZ1pyZUNhNUdFN3lGYTU3R3BhSzI1aXBrZHRqIiwic2VuZGVySWQiOiIiLCJuYmYiOjE2MzQ4MzQ5MzEsInNjb3BlIjoiIiwiYXBwbGljYXRpb25JZCI6IkFDVUkiLCJleHAiOjE2MzQ4Mzg1MzEsImlhdCI6MTYzNDgzNDkzMSwiY2hhbm5lbElkIjoiIiwianRpIjoiMWU2ZjQ0MTQtNmZhNC1kODU1LTBjODUtNWUwN2FlNzNlY2M1In0.r7jM_-wAUZYrYAKH6gbkT9MtF2RlTC5IbOfx5ZZcHQNVkFJRBLhGcYa9QxR0AHNLe5vMKBu1bKBn_DKozfeRFsz69EV8zYTEDV0J8_KpBi6IZ1GzQCDZiR9LYZ9wtgSP-CndSV5TFSfrs8c3dUyRdci0QTvqhXCqhz-jyjtRRZ7EpLzYs3bEG7ZZ0dGPeDE-X8nMBdlUs6oBaN2AyiG2bP42wsNoU4yTf7739jsA8hqkuF3rSfHNNJLVaI_iBdOqc5C3Ta8uJQbQeVEb3dh166hFxGEEvgqpZLQ4lVYib_gVxxtb8rWt_C3pDK80VF9BRrv2dPWczOz82DU0Tgxo2A",
				applicationid: "ATLAS",
				channelid: "CARE",
				interactionid: uuid.v4(),
				"x-auth-originator": "eyJraWQiOiI0Nzc1M2UzZi0zOGFjLTQ1ODQtZDcxYy0zZDgxYTE1Nzk5NWMiLCJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJzdWIiOiJBejR0MWdacmVDYTVHRTd5RmE1N0dwYUsyNWlwa2R0aiIsInJ0Ijoie1wic2VnbWVudGF0aW9uSWRcIjpcIlBPTEFSSVNcIn0iLCJkZWFsZXJDb2RlIjoiIiwiaXNzIjoiaHR0cHM6XC9cL3FhdDAxLmFwaS50LW1vYmlsZS5jb21cL29hdXRoMlwvdjYiLCJtYXN0ZXJEZWFsZXJDb2RlIjoiIiwiYXV0aFRpbWUiOiIxNjM0ODM0OTMxMTY3Iiwic3RvcmVJZCI6IiIsInVzbiI6IjZmOTRiNTE4LWFiYzUtYTgyYy01NzZjLWE4MjcwZGRmM2M0ZiIsImF1ZCI6IkF6NHQxZ1pyZUNhNUdFN3lGYTU3R3BhSzI1aXBrZHRqIiwic2VuZGVySWQiOiIiLCJuYmYiOjE2MzQ4MzQ5MzEsInNjb3BlIjoiIiwiYXBwbGljYXRpb25JZCI6IkFDVUkiLCJleHAiOjE2MzQ4Mzg1MzEsImlhdCI6MTYzNDgzNDkzMSwiY2hhbm5lbElkIjoiIiwianRpIjoiMWU2ZjQ0MTQtNmZhNC1kODU1LTBjODUtNWUwN2FlNzNlY2M1In0.r7jM_-wAUZYrYAKH6gbkT9MtF2RlTC5IbOfx5ZZcHQNVkFJRBLhGcYa9QxR0AHNLe5vMKBu1bKBn_DKozfeRFsz69EV8zYTEDV0J8_KpBi6IZ1GzQCDZiR9LYZ9wtgSP-CndSV5TFSfrs8c3dUyRdci0QTvqhXCqhz-jyjtRRZ7EpLzYs3bEG7ZZ0dGPeDE-X8nMBdlUs6oBaN2AyiG2bP42wsNoU4yTf7739jsA8hqkuF3rSfHNNJLVaI_iBdOqc5C3Ta8uJQbQeVEb3dh166hFxGEEvgqpZLQ4lVYib_gVxxtb8rWt_C3pDK80VF9BRrv2dPWczOz82DU0Tgxo2A",
			};
		},
	},

	/**
	 * Fired after database connection establishing.
	 */
	async afterConnected() {
		// await this.adapter.collection.createIndex({ name: 1 });
	}    
};