var addToCart = function addToCart(skuCode) {
	var btn = document.getElementById('btn-' + skuCode);
	btn.disabled = true;
	btn.classList.add('spin');
	// TODO: Remove this fake api.
	axios.get('https://localhost:4000/api/v1/carts').then(function (response) {
		console.log('Successfully added to cart', response);
		btn.classList.remove('spin');
		btn.disabled = false;
	}).catch(function (error) {
		// btn.classList.remove('spin');
		// btn.disabled = false;
		console.log('Error', error);
		console.log('Sku code', skuCode);
	});
};

var DeviceCard = function DeviceCard(props) {
	var device = props.device;
	var formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' });
	var baseUrl = 'https://cdn.tmobile.com';
	// const thumb = product.thumbNail.url
	var quantityOptions = Array.from({ length: 5 }, function (v, k) {
		return k + 1;
	});
	return React.createElement(
		'div',
		{ key: device.skuCode, className: 'col-md-4 mb-5' },
		React.createElement(
			'div',
			{ className: 'card fuse-card' },
			React.createElement(
				'div',
				{ className: 'card-body card-content' },
				React.createElement(
					'div',
					{ className: 'd-flex justify-content-between align-items-center' },
					React.createElement(
						'div',
						{ className: 'mt-5' },
						React.createElement(
							'h5',
							{ className: 'text-uppercase mb-0' },
							device.manufacturer
						),
						React.createElement(
							'h1',
							{ className: 'main-heading mt-0' },
							device.familyName
						),
						React.createElement(
							'div',
							{ className: 'd-flex flex-row' },
							React.createElement(
								'h6',
								{ className: 'text-muted ml-1' },
								formatter.format(device.price)
							)
						)
					),
					React.createElement('div', { className: 'image' }),
					React.createElement('img', { src: baseUrl + device.thumbNail.url, width: 150 })
				),
				React.createElement(
					'div',
					{ className: 'd-flex justify-content-between align-items-center mt-2 mb-2' },
					React.createElement(
						'span',
						null,
						'Get it ',
						new Date(device.fromDate).toLocaleString('default', { month: 'short', day: '2-digit' }),
						' -',
						new Date(device.toDate).toLocaleString('default', { month: 'short', day: '2-digit' })
					)
				),
				React.createElement(
					'div',
					{ className: 'row mx-auto justify-content-between align-items-center' },
					React.createElement(
						'label',
						{ htmlFor: 'quantity' },
						'Qty:'
					),
					React.createElement(
						'select',
						{ style: { width: '25%' } },
						quantityOptions.map(function (o) {
							return React.createElement(
								'option',
								{ key: o },
								o
							);
						})
					),
					React.createElement(
						'button',
						{
							className: 'btn bg-magenta',
							type: 'button',
							onClick: function onClick() {
								return addToCart(device.skuCode);
							},
							id: 'btn-' + device.skuCode
						},
						'Add to Cart ',
						React.createElement('span', { className: 'spinner' })
					)
				)
			)
		)
	);
};

var DeviceList = function DeviceList() {
	// const { items } = useSearch();
	var _useBoth = useBoth(),
	    items = _useBoth.items;

	return React.createElement(
		React.Fragment,
		null,
		items.map(function (i) {
			return React.createElement(DeviceCard, { key: i.skuCode, device: i });
		})
	);
};

var element = React.createElement(DeviceList, null);
ReactDOM.render(element, document.getElementById('results'));