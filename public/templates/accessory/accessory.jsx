const addToCart = (skuCode) => {
	const btn = document.getElementById(`btn-${skuCode}`);
	btn.disabled = true;
	btn.classList.add('spin');
	// TODO: Remove this fake api.
	axios
		.get('https://localhost:4000/api/v1/carts')
		.then((response) => {
			console.log(`Successfully added to cart`, response);
			btn.classList.remove('spin');
			btn.disabled = false;
		})
		.catch((error) => {
			// btn.classList.remove('spin');
			// btn.disabled = false;
			console.log('Error', error);
			console.log('Sku code', skuCode);
		});
};

const AccessoryCard = (props) => {
	const accessory = props.accessory;
	const formatter = new Intl.NumberFormat('en-US', { style: 'currency', currency: 'USD' });
	const baseUrl = 'https://cdn.tmobile.com';
	// const thumb = product.thumbNail.url
	const quantityOptions = Array.from({ length: 5 }, (v, k) => k + 1);
	return (
		<div key={accessory.skuCode} className="col-md-4 mb-5">
			<div className="card fuse-card">
				<div className="card-body card-content">
					<h2>TOTALLY NOT THE SAME AS DEVICE TEMPLATE</h2>
					<div className="d-flex justify-content-between align-items-center">
						<div className="mt-5">
							<h5 className="text-uppercase mb-0">{accessory.manufacturer}</h5>
							<h1 className="main-heading mt-0">{accessory.familyName}</h1>
							<div className="d-flex flex-row">
								<h6 className="text-muted ml-1">{formatter.format(accessory.price)}</h6>
							</div>
						</div>
						<div className="image" />
						<img src={baseUrl + accessory.thumbNail.url} width={150} />
					</div>
					<div className="d-flex justify-content-between align-items-center mt-2 mb-2">
						<span>
							Get it {new Date(accessory.fromDate).toLocaleString('default', { month: 'short', day: '2-digit' })} -
							{new Date(accessory.toDate).toLocaleString('default', { month: 'short', day: '2-digit' })}
						</span>
					</div>
					<div className="row mx-auto justify-content-between align-items-center">
						<label htmlFor="quantity">Qty:</label>
						<select style={{ width: '25%' }}>
							{quantityOptions.map((o) => (
								<option key={o}>{o}</option>
							))}
						</select>
						<button
							className="btn bg-magenta"
							type="button"
							onClick={() => addToCart(accessory.skuCode)}
							id={`btn-${accessory.skuCode}`}
						>
							Add to Cart <span className="spinner" />
						</button>
					</div>
				</div>
			</div>
		</div>
	);
};

const AccessoryList = () => {
	// const { items } = useSearch();
	const { items } = useBoth();
	return (
		<React.Fragment>
			{items.map((i) => {
				return <AccessoryCard key={i.skuCode} accessory={i} />;
			})}
		</React.Fragment>
	);
};

const element = <AccessoryList />;
ReactDOM.render(element, document.getElementById('results'));
