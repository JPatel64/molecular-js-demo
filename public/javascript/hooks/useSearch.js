const e = React.createElement;

const useSearch = () => {
	const [items, setItems] = React.useState([...products]);
	React.useEffect(() => {
		const searchToState = (event) => {
			setItems(event.detail);
		};
		document.addEventListener('search_results', searchToState);
		return () => document.removeEventListener('search_results', searchToState);
	}, []);

	return { items, setItems };
};

const useFilter = () => {
	let [items, setItems] = React.useState([...products]);

	React.useEffect(() => {
		const searchToState = (event) => {
			setItems(event.detail);
		};
		document.addEventListener('filter_results', searchToState);
		return () => document.removeEventListener('filter_results', searchToState);
	}, []);

	return { items, setItems };
};

const useBoth = () => {
	let [items, setItems] = React.useState([...products]);

	React.useEffect(() => {
		const searchToState = (event) => {
			setItems(event.detail);
		};
		document.addEventListener('search_results', searchToState);
		return () => document.removeEventListener('search_results', searchToState);
	}, []);

	React.useEffect(() => {
		const searchToState = (event) => {
			setItems(event.detail);
		};
		document.addEventListener('filter_results', searchToState);
		return () => document.removeEventListener('filter_results', searchToState);
	}, []);
	return { items, setItems };
};
