const getManufacturers = () => {
	return Array.from(new Set(products.map((p) => p.manufacturer))).sort((a, b) => a.localeCompare(b));
};

const filterByManufacturer = () => {
	const selectedManufacturers = [...document.querySelectorAll("input.filter-manufacturer[type='checkbox'")]
		.filter((input) => input.checked)
		.map((input) => input.value);
	const results = products.filter((p) => selectedManufacturers.includes(p.manufacturer));
	const event = new CustomEvent('filter_results', { detail: results });
	document.dispatchEvent(event);
};

const FilterByManufacturer = () => {
	const manufacturers = getManufacturers();
	return (
		<React.Fragment>
			{manufacturers.map((m) => {
				return (
					<label key={m} className="form-label">
						<span className="tmo-checkbox">
							<input
								className="filter-manufacturer"
								type="checkbox"
								defaultValue={m}
								onChange={() => filterByManufacturer()}
							/>
							<span className="checkbox">
								<i className="fa fa-check" />
							</span>
						</span>
						<p className="content">{m}</p>
					</label>
				);
			})}
		</React.Fragment>
	);
};

const element = <FilterByManufacturer />;
ReactDOM.render(element, document.getElementById('filter-by-manufacturer'));
