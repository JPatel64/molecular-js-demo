function _toConsumableArray(arr) {
	if (Array.isArray(arr)) {
		for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) {
			arr2[i] = arr[i];
		}
		return arr2;
	} else {
		return Array.from(arr);
	}
}

var getManufacturers = function getManufacturers() {
	return Array.from(
		new Set(
			products.map(function (p) {
				return p.manufacturer;
			})
		)
	).sort(function (a, b) {
		return a.localeCompare(b);
	});
};

var filterByManufacturer = function filterByManufacturer() {
	var selectedManufacturers = []
		.concat(_toConsumableArray(document.querySelectorAll("input.filter-manufacturer[type='checkbox'")))
		.filter(function (input) {
			return input.checked;
		})
		.map(function (input) {
			return input.value;
		});
	var results = products.filter(function (p) {
		return selectedManufacturers.includes(p.manufacturer);
	});
	var event = new CustomEvent('filter_results', { detail: results });
	document.dispatchEvent(event);
};

var FilterByManufacturer = function FilterByManufacturer() {
	var manufacturers = getManufacturers();
	return React.createElement(
		React.Fragment,
		null,
		manufacturers.map(function (m) {
			return React.createElement(
				'label',
				{ key: m, className: 'form-label' },
				React.createElement(
					'span',
					{ className: 'tmo-checkbox' },
					React.createElement('input', {
						className: 'filter-manufacturer',
						type: 'checkbox',
						defaultValue: m,
						onChange: function onChange() {
							return filterByManufacturer();
						},
					}),
					React.createElement('span', { className: 'checkbox' }, React.createElement('i', { className: 'fa fa-check' }))
				),
				React.createElement('p', { className: 'content' }, m)
			);
		})
	);
};

var element = React.createElement(FilterByManufacturer, null);
ReactDOM.render(element, document.getElementById('filter-by-manufacturer'));
