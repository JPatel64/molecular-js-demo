function addToCart(product) {
	let btn = document.getElementById(`btn-${product.skuId}`);
	btn.disabled = true;
	btn.classList.add('spin');
	// TODO: Remove this fake api.
	axios
		.get('https://localhost:4000/api/v1/carts')
		.then((response) => {
			console.log(`Successfully added to cart`, response);
			btn.classList.remove('spin');
			btn.disabled = false;
		})
		.catch((error) => {
			// btn.classList.remove('spin');
			// btn.disabled = false;
			console.log('Error', error);
			console.log('Product', product);
		});
}
