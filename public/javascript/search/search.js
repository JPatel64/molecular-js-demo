import { doSearch } from './search.util.js';

var Search = function Search() {
	return React.createElement(
		React.Fragment,
		null,
		React.createElement(
			'div',
			{ className: 'search-box search' },
			React.createElement(
				'fieldset',
				{ className: 'form-group' },
				React.createElement('input', {
					className: 'form-control',
					id: 'search',
					type: 'search',
					autoComplete: 'off',
					onKeyUp: function onKeyUp() {
						return doSearch();
					},
				}),
				React.createElement('label', { htmlFor: 'search' }, 'Search')
			)
		)
	);
};

var element = React.createElement(Search, null);
ReactDOM.render(element, document.getElementById('search-box'));
