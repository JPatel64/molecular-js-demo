import { search } from './search.util.js';
import { devices } from '../../../../mockData/devices.js';

describe('search', () => {
	const array = devices;

	it('should filter array based on name', () => {
		const results = search(array, ['galaxy']);
		expect(results.length).toBe(7);
		results.forEach((result) => {
			expect(result.familyName).toEqual(expect.stringContaining('Galaxy'));
		});
	});

	it('should filter array based on manufacturer', () => {
		const results = search(array, ['oneplus']);
		expect(results.length).toBe(6);
		results.forEach((result) => {
			expect(result.manufacturer).toEqual(expect.stringContaining('OnePlus'));
		});
	});

	it('should filter array based on memory options', () => {
		const results = search(array, ['256gb']);
		expect(results.length).toBe(3);
		results.forEach((result) => {
			expect(result.memoryOptions).toEqual(
				expect.arrayContaining([
					{
						name: '256GB',
					},
				])
			);
		});
	});

	it('should filter array based on color options', () => {
		const results = search(array, ['gray']);
		expect(results.length).toBe(1);
		results.forEach((result) => {
			result.colorOptions.forEach((co) => {
				expect(co.name).toEqual(expect.stringContaining('Volcanic Gray'));
			});
		});
	});

	it('should filter array based on SKU', () => {
		const results = search(array, ['610214661180']);
		expect(results.length).toBe(1);
		results.forEach((result) => {
			expect(result.skuCode).toEqual(expect.stringContaining('610214661180'));
		});
	});

	it('should filter array based on multiple search terms', () => {
		const results = search(array, ['610214661180', 'gray', '256gb', 'oneplus', 'galaxy']);
		expect(results.length).toBe(14);
	});
});
