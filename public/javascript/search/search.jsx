import { doSearch } from './search.util.js';

const Search = () => {
	return (
		<React.Fragment>
			<div className="search-box search">
				<fieldset className="form-group">
					<input className="form-control" id="search" type="search" autoComplete="off" onKeyUp={() => doSearch()} />
					<label htmlFor="search">Search</label>
				</fieldset>
			</div>
		</React.Fragment>
	);
};

const element = <Search />;
ReactDOM.render(element, document.getElementById('search-box'));
