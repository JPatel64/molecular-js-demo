export const doSearch = () => {
	const term = document
		.getElementById('search')
		.value.toLowerCase()
		.split(' ')
		.filter((e) => String(e).trim());

	const results = term.length ? search(products, term) : products;
	const event = new CustomEvent('search_results', { detail: results });
	document.dispatchEvent(event);
};

export const search = (array, terms) => {
	const regexList = terms.map((t) => new RegExp(t, 'gi'));

	const check = (field) => {
		if (field !== null && typeof field === 'object') {
			return Object.values(field).some(check);
		}

		if (Array.isArray(field)) {
			return obj.some(check);
		}

		return typeof field === 'string' && regexList.some((rx) => rx.test(field));
	};

	return array.filter(check);
};
